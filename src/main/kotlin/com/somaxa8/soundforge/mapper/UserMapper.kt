package com.somaxa8.soundforge.mapper

import com.somaxa8.soundforge.domain.models.User
import org.mapstruct.*

@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
interface UserMapper {

    @Mapping(target = "password", ignore = true)
    fun fromUserToUser(target: User, @MappingTarget source: User): User

}
