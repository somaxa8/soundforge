package com.somaxa8.soundforge.controller

import com.somaxa8.shared.web.annotation.Paged
import com.somaxa8.shared.web.annotation.Pager
import com.somaxa8.soundforge.domain.models.Sound
import com.somaxa8.soundforge.security.SessionManager
import com.somaxa8.soundforge.service.SoundService
import jakarta.validation.Valid
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

@RestController
class SoundController(
    private val _soundService: SoundService,
    private val _sessionManager: SessionManager,
) {

    @GetMapping("/sounds/{id}")
    fun getById(@PathVariable id: Long): Sound {
        return _soundService.findById(id)
    }

    @Paged
    @GetMapping("/sounds")
    fun findAll(
        @ModelAttribute sound: Sound,
        @Pager pageRequest: PageRequest,
    ): Page<Sound> {
        return _soundService.findAll(sound, pageRequest)
    }

    @PostMapping("/sounds")
    fun create(@Valid @ModelAttribute sound: Sound): Sound {
        return _soundService.create(sound)
    }

    @PreAuthorize("@_sessionManager.requestingSoundMatchesWithSessionId(#id) or hasAnyAuthority('ADMIN', 'SUPER_ADMIN')")
    @PatchMapping("/sounds/{id}")
    fun update(
        @PathVariable id: Long,
        @Valid @RequestBody sound: Sound,
    ): Sound {
        return _soundService.update(id, sound)
    }

    @PreAuthorize("@_sessionManager.requestingSoundMatchesWithSessionId(#id) or hasAnyAuthority('ADMIN', 'SUPER_ADMIN')")
    @DeleteMapping("/sounds/{id}")
    fun delete(@PathVariable id: Long) {
        return _soundService.deleteById(id)
    }
}