package com.somaxa8.soundforge.controller

import com.somaxa8.shared.web.annotation.Paged
import com.somaxa8.shared.web.annotation.Pager
import com.somaxa8.soundforge.domain.models.SoundLibrary
import com.somaxa8.soundforge.security.SessionManager
import com.somaxa8.soundforge.service.SoundLibraryService
import jakarta.validation.Valid
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

@RestController
class SoundLibraryController(
    private val _soundLibraryService: SoundLibraryService,
    private val _sessionManager: SessionManager,
) {

    @GetMapping("sound-libraries/{id}")
    fun getById(@PathVariable id: Long): SoundLibrary {
        return _soundLibraryService.findById(id)
    }

    @Paged
    @GetMapping("/sound-libraries")
    fun findAll(
        @ModelAttribute soundLibrary: SoundLibrary,
        @Pager pageRequest: PageRequest,
    ): Page<SoundLibrary> {
        return _soundLibraryService.findAll(soundLibrary, pageRequest)
    }

    @PostMapping("/sound-libraries")
    fun create(@Valid @RequestBody soundLibrary: SoundLibrary): SoundLibrary {
        return _soundLibraryService.create(soundLibrary)
    }

    @PreAuthorize("@_sessionManager.requestingSoundLibraryMatchesWithSessionId(#id) or hasAnyAuthority('ADMIN', 'SUPER_ADMIN')")
    @PatchMapping("/sound-libraries/{id}")
    fun update(
        @PathVariable id: Long,
        @Valid @RequestBody soundLibrary: SoundLibrary,
    ): SoundLibrary {
        return _soundLibraryService.update(id, soundLibrary)
    }

    @PreAuthorize("@_sessionManager.requestingSoundLibraryMatchesWithSessionId(#id) or hasAnyAuthority('ADMIN', 'SUPER_ADMIN')")
    @DeleteMapping("/sound-libraries/{id}")
    fun delete(@PathVariable id: Long) {
        return _soundLibraryService.deleteById(id)
    }
}