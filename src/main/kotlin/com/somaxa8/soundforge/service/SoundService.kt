package com.somaxa8.soundforge.service

import com.somaxa8.shared.utils.BaseService
import com.somaxa8.soundforge.domain.models.File
import com.somaxa8.soundforge.domain.models.Sound
import com.somaxa8.soundforge.repository.SoundRepository
import org.springframework.stereotype.Service

@Service
class SoundService(
    private val _soundRepository: SoundRepository,
    private val _fileService: FileService,
) : BaseService<Sound, Long>(_soundRepository) {

    override fun beforeCreate(entity: Sound): Sound {
        val file = _fileService.create(
            File(file = entity.soundFile, type = File.Type.SOUND)
        )

        entity.fileId = file.id
        entity.soundFile = null

        return entity
    }

    override fun beforeUpdate(id: Long, entity: Sound): Sound {
        super.beforeUpdate(id, entity)

        entity.soundFile?.let {
            val file = _fileService.update(entity.fileId!!, File(file = it))
            entity.fileId = file.id
        }

        return entity
    }

    override fun beforeDelete(id: Long) {
        val sound = findById(id)

        _fileService.deleteById(sound.fileId!!)
    }
}