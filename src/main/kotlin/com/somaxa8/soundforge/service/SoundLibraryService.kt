package com.somaxa8.soundforge.service

import com.somaxa8.shared.utils.BaseService
import com.somaxa8.soundforge.domain.models.SoundLibrary
import com.somaxa8.soundforge.repository.SoundLibraryRepository
import org.springframework.stereotype.Service

@Service
class SoundLibraryService(
    private val _soundLibraryRepository: SoundLibraryRepository,
) : BaseService<SoundLibrary, Long>(_soundLibraryRepository) {
}