package com.somaxa8.soundforge

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.EnableAspectJAutoProxy
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.stereotype.Repository

@SpringBootApplication
@EnableAspectJAutoProxy
@ComponentScan("com.somaxa8")
@EnableJpaRepositories(includeFilters = [ComponentScan.Filter(Repository::class)])
class DemoApplication

fun main(args: Array<String>) {
	runApplication<DemoApplication>(*args)
}
