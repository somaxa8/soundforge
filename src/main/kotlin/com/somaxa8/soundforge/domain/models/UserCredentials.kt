package com.somaxa8.soundforge.domain.models

import jakarta.validation.constraints.NotEmpty

class UserCredentials(
    @field:NotEmpty
    var email: String,

    @field:NotEmpty
    var password: String
)