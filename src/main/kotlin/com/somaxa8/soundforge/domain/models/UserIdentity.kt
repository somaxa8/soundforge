package com.somaxa8.soundforge.domain.models

class UserIdentity(
    var email: String,
    var id: Long,
)