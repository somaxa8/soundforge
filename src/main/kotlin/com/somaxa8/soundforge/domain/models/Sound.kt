package com.somaxa8.soundforge.domain.models

import com.fasterxml.jackson.annotation.JsonIgnore
import com.somaxa8.shared.utils.Identifiable
import com.somaxa8.shared.validation.groups.Create
import com.somaxa8.shared.web.annotation.EntityExists
import com.somaxa8.shared.web.constraints.ValidFileExtensions
import jakarta.persistence.*
import org.jetbrains.annotations.NotNull
import org.springframework.web.multipart.MultipartFile

@Entity
class Sound(
    @Id
    @GeneratedValue
    override var id: Long? = null,

    @field:NotNull
    @Column(nullable = false)
    var name: String? = null,

    var icon: String? = null,

    @field:NotNull
    @field:ValidFileExtensions(extensions = ["mp3"])
    @Transient
    var soundFile: MultipartFile? = null,

    @field:NotNull
//    @field:EntityExists(
//        entityName = "SoundLibrary",
//        primaryKey = "id",
//    )
    @Column(name = "sound_library_id", nullable = false)
    var soundLibraryId: Long? = null,

): Identifiable<Long>, Auditing() {
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "sound_library_id", insertable = false, nullable = false, updatable = false)
    var soundLibrary: SoundLibrary? = null

    @Column(name = "file_id", nullable = false)
    var fileId: Long? = null

    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "file_id", insertable = false, updatable = false)
    var file: File? = null

    @get:Transient
    val fileUrl: String?
        get() = file?.url
}