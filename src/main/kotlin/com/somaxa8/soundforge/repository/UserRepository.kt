package com.somaxa8.soundforge.repository

import com.somaxa8.soundforge.domain.models.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
@Repository
interface UserRepository: JpaRepository<User, Long>{

    fun findByEmail(email: String): User
    fun existsByEmail(email: String): Boolean

}
