package com.somaxa8.soundforge.repository

import com.somaxa8.soundforge.domain.models.File
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface FileRepository : JpaRepository<File, Long> {
}