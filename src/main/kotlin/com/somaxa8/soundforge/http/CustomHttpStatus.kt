package com.somaxa8.soundforge.http

enum class CustomHttpStatus(val value: Int) {

    CREDENTIALS_EXPIRED(499); // Series.CLIENT_ERROR, "Credentials Expired")

    fun value(): Int {
        return this.value
    }

}