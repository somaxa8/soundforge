package com.somaxa8.shared.web.aspect

import jakarta.servlet.http.HttpServletResponse
import org.aspectj.lang.ProceedingJoinPoint
import org.aspectj.lang.annotation.Around
import org.aspectj.lang.annotation.Aspect
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.stereotype.Component

@Aspect
@Component
class PagedAspect {

    companion object {
        const val X_TOTAL_COUNT_HEADER = "X-Total-Count"
        const val X_TOTAL_PAGES_HEADER = "X-Pager-Total-Pages"
        const val X_PAGE_SIZE_HEADER = "X-Pager-Page-Size"
    }

    @Autowired
    private lateinit var response: HttpServletResponse

    @Around("@annotation(com.somaxa8.shared.web.annotation.Paged)")
    fun injectValue(joinPoint: ProceedingJoinPoint): Any {
        val result = joinPoint.proceed()

        if (result is Page<*>) {
            response.addHeader(X_TOTAL_COUNT_HEADER, result.totalElements.toString())
            response.addHeader(X_TOTAL_PAGES_HEADER, result.totalPages.toString())
            response.addHeader(X_PAGE_SIZE_HEADER, result.size.toString())
        }

        return result
    }

}