package com.somaxa8.shared.persistence.utils

import org.springframework.data.domain.PageImpl
import org.springframework.data.domain.PageRequest
import jakarta.persistence.EntityManager
import jakarta.persistence.Tuple
import jakarta.persistence.criteria.CriteriaQuery

object CriteriaUtil {

    fun <T> page(entityManager: EntityManager, query: CriteriaQuery<Tuple>, pageRequest: PageRequest): PageImpl<T> {
        val typedQuery = entityManager.createQuery(query)
        typedQuery.firstResult = pageRequest.pageNumber * pageRequest.pageSize
        typedQuery.maxResults = pageRequest.pageSize

        val tupleResult = typedQuery.resultList
        val totalCount = tupleResult[0][0] as Long
        val resultList = tupleResult.map { it[1] as T }

        return PageImpl(resultList, pageRequest, totalCount)
    }


    fun <T> page(entityManager: EntityManager, query: CriteriaQuery<T>, pageRequest: PageRequest, totalCount: Long): PageImpl<T> {
        val resultQuery = entityManager.createQuery(query)
        resultQuery.firstResult = pageRequest.pageNumber * pageRequest.pageSize
        resultQuery.maxResults = pageRequest.pageSize

        return PageImpl(resultQuery.resultList, pageRequest, totalCount)
    }
}