<!---------------------------------------------------------------------------->
<!-- Describe the requirement -->

{DESCRIPTION}

<!---------------------------------------------------------------------------->
# Must-have checklist
<!--
Which criteria should the implementation have to be considered done?
-->

* [ ] First criteria.
* [ ] Second criteria.

<!---------------------------------------------------------------------------->
# Additional info
<!--........................................................................-->
## Design Proposal

{C4 MODEL, DIAGRAMS}

<!--........................................................................-->
## Design Notes

{NOTES}

<!---------------------------------------------------------------------------->
<!-- Please don't change this lines -->
/label ~"Type::Refactorization

