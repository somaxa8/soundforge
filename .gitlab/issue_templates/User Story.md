<!---------------------------------------------------------------------------->
<!-- Describe the new feature -->

**User Story Legend**:

> As a **{ROLE: Who wants to accomplish something}**, I want to **{ACTION: What
> they want to accomplish}** so that **{OBJECTIVE: Why they want to accomplish
> that thing}**

{DESCRIPTION}

<!---------------------------------------------------------------------------->
# Tasks
<!--
Define one by one the tasks to finish (implement) this issue.
Format should be:

* [ ] Task name `estimated time expressed in min or h`

If the task is longer than a day, it should be splitted into different
*issues**.
-->

* [ ] Task 1 `1h`
* [ ] Task 2 `30min`
* [ ] Task 3 `1.5h`

<!---------------------------------------------------------------------------->
# Must-have checklist
<!--
Which criteria should the implementation have to be considered done?
-->

* [ ] First criteria.
* [ ] Second criteria.

<!---------------------------------------------------------------------------->
# Additional info
<!--........................................................................-->
## Design Proposal
<!--
NOTE: Screenshots, Mocks or Wireframes should be uploaded on the issue designs
section.
-->

{C4 MODEL, DIAGRAMS}

<!--........................................................................-->
## Design Notes

{DESIGN_NOTES}

<!---------------------------------------------------------------------------->
<!-- Please don't change this lines -->
/label ~"Type::UserStory"

